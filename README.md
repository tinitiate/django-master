# Django Master Tutorials
> Venkata Bhattaram (c) Syntax Board Inc

## Each Folder is a Django Project
* Download the files and in each of the `syntaxboard_django_??` folder there is a `manage.py` to run the project
* There is a readme MarkDown file with more details in each of the project
